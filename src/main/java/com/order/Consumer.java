package com.order;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.*;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;

import java.util.List;

public class Consumer {
    public static void main(String[] args) throws Exception {

        //创建一个消息接收对象consumer
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("group1");

        //设定接收消息的命名服务器地址---获取到消息服务器ip
        consumer.setNamesrvAddr("192.168.200.130:9876");

        //设置接收消息对应的topic，对应的sub标签为任意*，之前producer没有指定tag。如果producer发送的消息指定了tag，那么也必须指定相应的tag
        consumer.subscribe("orderTopic", "*");

        //开启监听，用于接收消息
        /*consumer.registerMessageListener(new MessageListenerConcurrently() {
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list, ConsumeConcurrentlyContext consumeConcurrentlyContext) {
                //遍历接收到的消息
                for (MessageExt msg : list) {
                    //      System.out.println("msg = " + msg);
                    System.out.println(Thread.currentThread().getName() + "消息为：" + new String(msg.getBody()));
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });*/

        //使用单线程的模式从消息队列中取数据，一个线程绑定一个消息队列
        consumer.registerMessageListener(new MessageListenerOrderly() {
            //使用ConsumeListenerOrderly后,对消息队列的处理由一个消息队列多个线程服务，转换为一个消息队列一个线程服务
            public ConsumeOrderlyStatus consumeMessage(List<MessageExt> list, ConsumeOrderlyContext consumeOrderlyContext) {
                //遍历接收到的消息
                for (MessageExt msg : list) {
                    System.out.println(Thread.currentThread().getName() + "消息为：" + new String(msg.getBody()));
                }
                return ConsumeOrderlyStatus.SUCCESS;
            }

        });


        //启动消息接收服务
        consumer.start();

    }

}
