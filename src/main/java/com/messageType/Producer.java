package com.messageType;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;

import java.util.concurrent.TimeUnit;

public class Producer {
    public static void main(String[] args) throws Exception {

        //创建发送消息对象
        DefaultMQProducer producer = new DefaultMQProducer("group1");

        //设定命名服务器地址---获取到消息服务器ip
        producer.setNamesrvAddr("192.168.200.130:9876");

        //启动发送服务
        producer.start();

        //同步消息
       /* for (int i = 1; i <= 10; i++) {
            //构建消息，指定topic和body
            Message msg = new Message("topic1", ("同步消息：hello"+i).getBytes());
            //发送消息
            SendResult sendResult = producer.send(msg);
            System.out.println("sendResult = " + sendResult);
        }*/


        //异步消息
    /*    for (int i = 1; i <= 10; i++) {

            //构建消息，指定topic和body
            Message msg = new Message("topic1", ("异步消息：hello" + i).getBytes());

            producer.send(msg, new SendCallback() {
                //消息发送成功
                public void onSuccess(SendResult sendResult) {
                    System.out.println("sendResult = " + sendResult);
                }

                //消息发送失败
                public void onException(Throwable t) {
                    System.out.println("t = " + t);
                }
            });
        }*/

        //单向消息
        //构建消息，指定topic和body
        Message msg = new Message("topic1", ("单向消息：hello").getBytes());
        producer.sendOneway(msg);

        //程序休眠10秒钟，确保异步消息返回后能够输出
        TimeUnit.SECONDS.sleep(10);

        //关闭连接
        producer.shutdown();

    }
}
