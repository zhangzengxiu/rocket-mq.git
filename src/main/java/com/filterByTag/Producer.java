package com.filterByTag;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;

import java.util.ArrayList;

public class Producer {
    public static void main(String[] args) throws Exception {

        //创建发送消息对象
        DefaultMQProducer producer = new DefaultMQProducer("group1");

        //设定命名服务器地址---获取到消息服务器ip
        producer.setNamesrvAddr("192.168.200.130:9876");

        //启动发送服务
        producer.start();

        //构建消息，指定topic和body
        Message msg = new Message("topic1", "tag1", "消息过滤tag1消息".getBytes());
        Message msg2 = new Message("topic1", "tag2", "消息过滤tag2消息".getBytes());

        //发送消息
        SendResult sendResult = producer.send(msg);
        SendResult sendResult2 = producer.send(msg2);

        System.out.println("sendResult = " + sendResult);

        //关闭连接
        producer.shutdown();

    }
}
